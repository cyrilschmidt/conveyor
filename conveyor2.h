#ifndef CONVEYOR2_H
#define CONVEYOR2_H

#include <atomic>
#include <boost/mpl/is_sequence.hpp>
#include <boost/mpl/begin_end.hpp>
#include <boost/mpl/next_prior.hpp>
#include <boost/mpl/deref.hpp>
#include <boost/mpl/assert.hpp>
#include <boost/type_traits/is_same.hpp>
#include <boost/mpl/vector_c.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/copy.hpp>
#include <boost/mpl/copy_if.hpp>
#include <boost/mpl/joint_view.hpp>
#include <boost/mpl/sort.hpp>
#include <boost/mpl/unique.hpp>
#include <boost/mpl/set.hpp>
#include <cstring>
#include <boost/type_traits/is_same.hpp>

namespace conv {

template <typename belt>
class belt_iterator;


template <typename belt, typename begin, typename end, bool>
struct min_barrier_impl
{
    static uint64_t execute(const belt&, uint64_t value)
    {
        return value;
    }
};

template <typename belt, typename begin, typename end>
struct min_barrier_impl<belt,begin,end,false>
{
    static uint64_t execute(const belt& b, uint64_t)
    {
        typedef typename boost::mpl::deref<begin>::type item;
        typedef typename boost::mpl::next<begin>::type next;
        uint64_t value = b.template get_barrier<item::value>();
        return std::min(value,
                        min_barrier_impl<belt,next,end,boost::is_same<next,end>::value>
                        ::execute(b, value));
    }
};


template <typename entry, uint64_t length, size_t num_barriers>
class belt
{
public:
    typedef entry value_type;
    typedef belt_iterator<belt<value_type, length, num_barriers> > iterator;

    belt()
    {
        std::memset(barriers, 0, sizeof(barriers));
    }

    template <size_t barrier>
    uint64_t get_barrier() const
    {
        return barriers[barrier].index.load(std::memory_order_acquire);
    }
    uint64_t wrap_around(uint64_t index) const
    {
        return index + length;
    }
    template <size_t barrier>
    uint64_t move_barrier(uint64_t steps)
    {
        uint64_t new_value = barriers[barrier].index.load(std::memory_order_relaxed) + steps;
        barriers[barrier].index.store(new_value, std::memory_order_release);
        return new_value;
    }
    value_type& operator[](uint64_t index)
    {
        return entries[index % length];
    }
    const value_type& operator[](uint64_t index) const
    {
        return entries[index % length];
    }
    template <typename sequence>
    uint64_t min_barrier()const
    {
        BOOST_MPL_ASSERT(( boost::mpl::is_sequence<sequence> ));

        typedef belt<entry,length,num_barriers> self;
        typedef typename boost::mpl::begin<sequence>::type begin;
        typedef typename boost::mpl::end<sequence>::type end;
        return min_barrier_impl<self,begin,end,boost::is_same<begin,end>::value>::execute(*this,0);
    }

private:
    static const uint64_t capacity = length;
    static const size_t   barrier_count = num_barriers;

    union barrier
    {
        std::atomic<uint64_t> index;
        char padding[64];
    };

    barrier barriers[barrier_count];
    entry entries[length];

    friend class belt_iterator<belt<value_type, capacity, barrier_count> >;
};


template <typename belt>
typename belt_iterator<belt>::difference_type operator - (const belt_iterator<belt>& lhs,
        const belt_iterator<belt>& rhs);

template <typename belt>
class belt_iterator : public std::random_access_iterator_tag
{
public:
    typedef typename belt::value_type value_type;
    typedef int64_t difference_type;
    typedef belt_iterator<belt> self;

    belt_iterator(belt& conv, uint64_t index)
        : conv(conv), index(index), offset(index % conv.capacity) {}

    self& operator++()
    {
        ++index;
        offset = index % belt::capacity;
        return *this;
    }

    self& operator+=(difference_type d)
    {
        index += d;
        offset = index % belt::capacity;
        return *this;
    }

    value_type& operator*()
    {
        return conv.entries[offset];
    }

private:
    belt& conv;
    uint64_t index;
    uint64_t offset;
    friend
    difference_type operator - <>(const belt_iterator<belt>& lhs,
                                  const belt_iterator<belt>& rhs);
};

template <typename belt>
typename belt_iterator<belt>::difference_type operator - (const belt_iterator<belt>& lhs,
        const belt_iterator<belt>& rhs)
{
    return lhs.index - rhs.index;
}

template <typename belt>
belt_iterator<belt> operator + (const belt_iterator<belt>& lhs,
                                        typename belt_iterator<belt>::difference_type rhs)
{
    belt_iterator<belt> result(lhs);
    return result += rhs;
}


template <typename belt, size_t barrier, typename sequence>
class producer
{
public:
    producer(belt& conv) : conv(conv) {}
    typedef typename belt::iterator iterator;
// Expected argument:
// uint64_t handler(belt::iterator begin, belt::iterator  end);
    template<class handler>
    void push(const handler& h)
    {
        uint64_t begin, end;

        do
        {
            //min_barrier<belt> f(conv);
            begin = conv.template get_barrier<barrier>();
            end = conv.wrap_around(conv.template min_barrier<sequence>());
            //rt_fold<sequence, min_barrier<belt>, uint64_t >(32535, f));
        }
        while (begin==end);   // belt is full

        // Invoke the handler to initialize the entries.
        // The handler returns the number of pushed entries.
        uint64_t count = h(typename belt::iterator(conv, begin),
                           typename belt::iterator(conv, end));
        conv.template move_barrier<barrier>(count);
    }
private:
    belt& conv;
};

// Holder for a particular thread id
template <size_t id>
struct thread_id
{
    static const size_t tid = id;
};

struct no_thread_id
{};

// Forward declaration
template <typename handler, typename thread, size_t barrier, typename barriers, bool producer>
class mapped_worker;

// This class only holds a handler
template <typename handler, typename thread = no_thread_id, size_t barrier = 0>
class worker
{
public:
    typedef thread tid;
    worker(handler h) : h(h) {} // todo: check that barrier == 0
    handler get_handler() { return h; }

    template <size_t new_barrier, typename barriers, bool producer> using mapped_type =
    mapped_worker<handler, thread, barrier, barriers, producer>;
    template <size_t new_barrier> using remapped_type =
    worker<handler, thread, new_barrier>;
    template <size_t new_barrier>
    remapped_type<new_barrier> remap() { return worker<handler, thread, new_barrier+barrier>(*this); }
    template <size_t new_barrier, typename barriers, bool producer>
    mapped_type<barrier, barriers, producer> map();
    static const size_t out_barrier = barrier;
    static const size_t next_barrier = barrier + 1;
    typedef boost::mpl::vector_c<size_t, barrier> out_barriers;
private:
    handler h;
};

// This class holds a handler and a thread id
template <typename handler, typename thread, size_t barrier, typename barriers, bool producer>
class mapped_worker
{
public:
    typedef thread tid;
    static const size_t out_barrier = barrier;
    static const size_t next_barrier = barrier + 1;
    typedef boost::mpl::vector_c<size_t, barrier> out_barriers;
    typedef barriers in_barriers;
    static const bool is_producer = producer;

    mapped_worker(worker<handler, thread> w) : h(w.get_handler()) {}
    handler get_handler() { return h; }
//    template <size_t new_barrier> using remapped_type =
//    mapped_worker<handler, thread, new_barrier>;
//    template <size_t new_barrier>
//    remapped_type<new_barrier> remap() { return worker<handler, thread, new_barrier+barrier>(*this); }
private:
    mapped_worker(handler h) : h(h) {}
    handler h;
};

template <typename handler, typename thread, size_t barrier>
template <size_t new_barrier, typename barriers, bool producer>
worker<handler, thread, barrier>::mapped_type<new_barrier+barrier, barriers, producer> worker<handler, thread, barrier>::map()
{ return mapped_type<new_barrier+barrier, barriers, producer>(*this); }

// Sequential execution of two workers
template <typename left, typename right>
class seq
{
private:
    left first;
    typename right::template mapped_type<left::next_barrier, typename left::out_barriers, false> second;
public:
    seq(left f, right s) :
        first(f),
        second(s.template map<left::next_barrier, typename left::out_barriers, false>())
        {}
};

struct my_handler {};

void f() {
  my_handler h1, h2;
  worker<my_handler> w1(h1);
  worker<my_handler> w2(h2);
  seq<worker<my_handler>, worker<my_handler> > s(w1, w2);
}

// Parallel execution of two workers
template <typename top, typename bottom>
class par
{
public:
    par(top first, bottom second) : first(first), second(second) {}
private:
    top first;
    bottom second;
};




// Gets the set of pre-set thread ids
template <typename t>
class get_thread_ids
{ // this should never be instantiated
};

template <typename handler>
class get_thread_ids<worker<handler, no_thread_id> >
{
public:
    typedef boost::mpl::vector<> ids;
};

template <typename handler, size_t barrier, typename barriers, bool producer>
class get_thread_ids<mapped_worker<handler, no_thread_id, barrier, barriers, producer> >
{
public:
    typedef boost::mpl::vector<> ids;
};

template <typename handler, size_t id>
class get_thread_ids<worker<handler, thread_id<id> > >
{
public:
    typedef boost::mpl::vector_c<size_t, id> ids;
};

template <typename handler, size_t id, size_t barrier, typename barriers, bool producer>
class get_thread_ids<mapped_worker<handler, thread_id<id>, barrier, barriers, producer> >
{
public:
    typedef boost::mpl::vector_c<size_t, id> ids;
};


template <typename seq1, typename seq2>
struct set_union
{
    typedef typename boost::mpl::sort<typename boost::mpl::copy<
        boost::mpl::joint_view<seq1, seq2>
      , boost::mpl::back_inserter< boost::mpl::vector<> >
    >::type>::type sorted;

    typedef typename boost::mpl::unique<sorted,
        boost::is_same<boost::mpl::_1, boost::mpl::_2> >::type type;
};



template <typename left, typename right>
class get_thread_ids<seq<left, right> >
{
public:
    typedef typename set_union<typename get_thread_ids<left>::ids,
                               typename get_thread_ids<right>::ids>::type ids;
};

template <typename top, typename bottom>
class get_thread_ids<par<top, bottom> >
{
public:
    typedef typename set_union<typename get_thread_ids<top>::ids,
                               typename get_thread_ids<bottom>::ids>::type ids;
};


// Runs the combination of workers


template <typename belt, size_t barrier, typename sequence>
class consumer
{
public:
    consumer(belt& conv) : conv(conv) {}
    typedef typename belt::iterator iterator;
// Expected argument:
// size_t handler(belt_iterator begin, belt_iterator end);
    template<class handler>
    void take(const handler& h)
    {
        uint64_t begin, end;

        do
        {
//        min_barrier<belt> f(conv);
            begin = conv.template get_barrier<barrier>();
//        end = rt_fold<sequence, min_barrier<belt>, uint64_t >(32535, f);
            end = conv.template min_barrier<sequence>();
        }
        while (begin==end);   // nothing to consume yet

        // Invoke the handler to initialize the entries.
        // The handler returns the number of pushed entries.
        uint64_t count = h(typename belt::iterator(conv, begin),
                           typename belt::iterator(conv, end));
        conv.template move_barrier<barrier>(count);
    }
// Expected argument:
// size_t handler(cbelt_iterator begin, belt_iterator end);
    template<class handler>
    void try_process(const handler& h)
    {
        uint64_t begin, end;
        begin = conv.template get_barrier<barrier>();
        end = conv.template min_barrier<sequence>();
        if(begin==end)  // nothing to consume yet
            return;

        // Invoke the handler to initialize the entries.
        // The handler returns the number of pushed entries.
        uint64_t count = h(typename belt::iterator(conv, begin),
                           typename belt::iterator(conv, end));
        conv.template move_barrier<barrier>(count);
    }
private:
    belt& conv;
};

}

#endif // CONVEYOR2_H
