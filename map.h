#ifndef MAP_H
#define MAP_H

#include <boost/mpl/map.hpp>
#include <boost/mpl/not.hpp>
#include <type_traits>

namespace conveyor {

namespace impl {

template <typename key, typename value, typename tail>
struct map : tail {
    typedef key key_type;
    typedef tail tail_type;

    map(const value& element_value, const tail& tail_value)
        : tail(tail_value), element_value(element_value) {}

    value element_value;

    template<typename get_key> using value_type = typename
        boost::mpl::if_<std::is_same<get_key, key>,
                        value,
                        typename tail::template value_type<get_key>
                       >::type;


    template<typename inserted_key, typename inserted_value> using insert_result = typename
        boost::mpl::if_<std::is_same<inserted_key, key>,
                        map<key, inserted_value, tail>,
                        map<key,
                                 value,
                                 typename tail::template insert_result<inserted_key, inserted_value>
                                >
                       >::type;
};

struct empty_map {
    template<typename key> using value_type = void;

    template<typename inserted_key, typename inserted_value> using insert_result =
        map<inserted_key, inserted_value, empty_map>;

    template<typename inserted_key, typename inserted_value>
    map<inserted_key, inserted_value, empty_map> insert(const inserted_value& inserted_element) const {
        return map<inserted_key, inserted_value, empty_map>(inserted_element, *this);
    }
};

template<typename inserted_key, typename inserted_value, typename map>
struct inserter {
    typedef typename map::template insert_result<inserted_key,inserted_value> result_type;
    static result_type
    insert(const inserted_value& v, const map& m) {
        return typename map::template insert_result<inserted_key,inserted_value>(m.element_value,
            inserter<inserted_key,inserted_value,typename map::tail_type>::insert(v, static_cast<typename map::tail_type const&>(m)));
    }
};

template<typename inserted_key, typename inserted_value, typename value, typename tail>
struct inserter<inserted_key, inserted_value, map<inserted_key, value, tail> > {
    typedef typename map<inserted_key, value, tail>::template insert_result<inserted_key,inserted_value> result_type;
    static result_type
    insert(const inserted_value& v, const map<inserted_key, value, tail>& m) {
        return map<inserted_key, inserted_value, tail>(v, static_cast<tail const&>(m));
    }
};

template<typename inserted_key, typename inserted_value>
struct inserter<inserted_key, inserted_value, empty_map> {
    typedef map<inserted_key, inserted_value, empty_map> result_type;
    static result_type
    insert(const inserted_value& v, const empty_map& m) {
        return map<inserted_key, inserted_value, empty_map>(v, m);
    }
};

template<typename key, typename value, typename map>
typename inserter<key, value, map>::result_type
insert(const value& v, const map& m) {
    return inserter<key, value, map>::insert(v, m);
}

template<typename key>
struct at {
template<typename value, typename tail>
static const value& get(const map<key,value,tail>& m) { return m.element_value; }
};

template<typename key, typename map>
struct has_key {
typedef typename boost::mpl::if_<
    std::is_same<key, typename map::key_type>,
    boost::mpl::true_,
    typename has_key<key, typename map::tail_type>::type>::type type;
};

template<typename key>
struct has_key<key, empty_map> {
typedef boost::mpl::false_ type;
};

// Iterators
template<typename map>
struct map_iterator {
    map_iterator(map&) {}
};

template<typename key, typename value, typename tail>
struct map_iterator<map<key, value, tail> >{
    map_iterator(map<key, value, tail>& container) : container(container) {}
    typedef value value_type;
    value& operator *() { return container.element_value; }
    const value& operator *() const { return container.element_value; }

    map_iterator<tail> operator ++ () { return map_iterator<tail>(container); }
private:
    map<key, value, tail>& container;
};

template<typename map>
map_iterator<map> begin(map& container) { return map_iterator<map>(container); }

} // namespace impl

}  // namespace conveyor

#endif // MAP_H

