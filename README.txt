Conveyor is a concurrency framework for C++, inspired by Disruptor.

Requirements: boost, googletest and autocheck.

To build:

mkdir build
cd build
cmake ..
make

To run the tests (in the build subdirectory):
./conveyor_test

An example of how to use the library is sgrep.cpp

The example code reads the standard input and prints 
the lines that match the given pattern.
In the build subdirectory, run for example
./sgrep '[iF].*[gH].*[vA]' <../LICENSE.txt






