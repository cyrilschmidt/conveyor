#ifndef WORKER_H
#define WORKER_H

namespace conveyor {

template <size_t id>
struct thread_id {
    static const size_t value = id;
};

struct no_thread_id {
};

template <typename handler, typename thread = no_thread_id, size_t barrier_count = 0, bool is_producer = false>
class worker_inst {
public:
    typedef thread id;
    typedef handler handler_type;
    static const size_t out_barrier_count = 1;
    static const bool producer = is_producer;
    size_t next_barrier() const { return out_barrier + 1; }
    barriers<1> get_out_barriers() const { barriers<1> result; result.value[0] = out_barrier; return result; }
    handler get_handler() const { return h; }
    const barriers<barrier_count>& get_in_barriers() const { return in_barriers; }
    size_t get_out_barrier() const { return out_barrier; }
    template <size_t count, bool is_prod = false> using mapped_type =
    worker_inst<handler, thread, count, is_prod>;

    worker_inst<handler, thread, barrier_count, is_producer>& shift(size_t new_barrier) {
        out_barrier += new_barrier;
        in_barriers += new_barrier;
        return *this;
    }
private:
    worker_inst(handler h, const barriers<barrier_count>& in_barriers, size_t out_barrier) : h(h), in_barriers(in_barriers), out_barrier(out_barrier) {}
    handler h;
    barriers<barrier_count> in_barriers;
    size_t out_barrier;
    friend class worker_inst<handler, thread, 0, false>;
};

template <typename handler, typename thread>
class worker_inst<handler, thread, 0, false> {
public:
    typedef thread id;
    typedef handler handler_type;
    static const bool producer = false;

    worker_inst(handler h) : h(h), out_barrier(0) {}

    static const size_t out_barrier_count = 1;

    size_t next_barrier() const { return out_barrier + 1; }

    barriers<1> get_out_barriers() const { barriers<1> result; result.value[0] = out_barrier; return result; }

    template <size_t count, bool is_producer = false> using mapped_type =
    worker_inst<handler, thread, count, is_producer>;

    template <size_t count, bool is_producer = false>
    worker_inst<handler, thread, count, is_producer> map(const barriers<count>& in_barriers) const
    { return worker_inst<handler, thread, count, is_producer>(h, in_barriers, out_barrier); }

    worker_inst<handler, thread, 0, false>& shift(size_t new_barrier) { out_barrier += new_barrier; return *this; }

private:
    handler h;
    size_t out_barrier;
};



template<typename handler>
worker_inst<handler> worker(handler h) { return worker_inst<handler>(h); }

template<size_t id, typename handler>
worker_inst<handler, thread_id<id> > worker(handler h) {
    return worker_inst<handler, thread_id<id> >(h);
}

template<typename iter, size_t arg1, typename func>
std::function<size_t(iter,iter)> apply(func f) {
    return [&](iter i1, iter i2) {
        typename iter::difference_type count = 0;
        for(; i1 != i2; ++i1, ++count)
            f(get<arg1>(*i1));
        return count;
    };
}

template<typename iter, size_t arg1, size_t result, typename func>
std::function<size_t(iter,iter)> apply(func f) {
    return [&](iter i1, iter i2) {
        typename iter::difference_type count = 0;
        for(; i1 != i2; ++i1, ++count)
            get<result>(*i1) = f(get<arg1>(*i1));
        return count;
    };
}


} // namespace conveyor

#endif // WORKER_H
