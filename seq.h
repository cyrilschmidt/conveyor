#ifndef SEQ_H
#define SEQ_H

#include "barriers.h"

namespace conveyor {

using impl::barriers;

// Sequential execution of two workers
template <typename left, typename right>
class seq
{
public:
    template <size_t count, bool is_producer = false> using mapped_type =
    seq<typename left::template mapped_type<count, is_producer>, right>;
    template <size_t count, bool is_producer = false>
    mapped_type<count, is_producer> map(const barriers<count>& in_barriers) const {
        return mapped_type<count, is_producer>(first.map<count, is_producer>(in_barriers), second);
    }

    static const size_t out_barrier_count = right::out_barrier_count;
    size_t next_barrier() const { return second.next_barrier(); }
    barriers<out_barrier_count> get_out_barriers() const { return second.get_out_barriers(); }

    seq<left, right>& shift(size_t new_barrier) {
        first.shift(new_barrier);
        second.shift(new_barrier);
        return *this;
    }
    seq(left first, right second) : first(first), second(second) {}

private:
    left first;
    right second;
public:
    template <typename visitor>
    auto apply(visitor& v) const -> decltype(v(first)(second)) { return v(first)(second); }
};


template <typename lhs, typename rhs>
seq<lhs, typename rhs::template mapped_type<lhs::out_barrier_count> >
create_seq(lhs f, rhs s) {
    return seq<lhs, typename rhs::template mapped_type<lhs::out_barrier_count> >(f,
        s.shift(f.next_barrier()).template map<lhs::out_barrier_count>(f.get_out_barriers()));
}

template <typename lhs, typename rhs>
auto operator >> (lhs f, rhs s) -> decltype (create_seq(f,s)) { return create_seq(f,s); }

}

#endif // SEQ_H
