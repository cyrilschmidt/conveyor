#ifndef THREAD_FUNC_H
#define THREAD_FUNC_H

#include <atomic>
#include <thread>
#include <deque>

#include "seq.h"
#include "par.h"
#include "worker.h"
#include "map.h"

namespace conveyor {

namespace impl {

class max_barrier_index {
public:
    max_barrier_index() : value(0) {}

    template <typename handler, typename thread_id, size_t in_barrier_count, bool is_producer>
    max_barrier_index& operator() (const worker_inst<handler, thread_id, in_barrier_count, is_producer>& w) {
        value = std::max(value, w.get_out_barrier());
        return *this;
    }

    template <typename left, typename right>
    auto operator() (const seq<left, right>& s) -> decltype(s.apply(*this)) { return s.apply(*this); }

    template <typename top, typename bottom>
    auto operator() (const par<top, bottom>& p) -> decltype(p.apply(*this)) { return p.apply(*this); }

    size_t get_value() const { return value; }
private:
    size_t value;
};

template<typename conveyor_type, typename handler, typename thread_id, size_t in_barrier_count>
bool worker_func_impl(conveyor_type& conveyor,
                          barrier_values& barriers,
                          const worker_inst<handler, thread_id, in_barrier_count, true>& w,
                          std::atomic<index_type>& out_barrier,
                          index_type upper, index_type lower) {
                        auto result = w.get_handler()(belt_iterator<conveyor_type>(conveyor, lower),
                            belt_iterator<conveyor_type>(conveyor, upper));
                        if(!std::get<1>(result)) // if the handler returns <_, false>, mark the end of the conveyor
                            barriers.stop.index.store(lower+std::get<0>(result), std::memory_order_release);
                        out_barrier.store(lower+std::get<0>(result), std::memory_order_release);
                        return std::get<1>(result); // if the handler returns <_, false>, exit the thread
}

template<typename conveyor_type, typename handler, typename thread_id, size_t in_barrier_count>
bool worker_func_impl(conveyor_type& conveyor,
                          barrier_values& barriers,
                          const worker_inst<handler, thread_id, in_barrier_count, false>& w,
                          std::atomic<index_type>& out_barrier,
                          index_type upper, index_type lower) {
                        auto result = w.get_handler()(belt_iterator<conveyor_type>(conveyor, lower),
                            belt_iterator<conveyor_type>(conveyor, upper));
                        out_barrier.store(lower+result, std::memory_order_release);
                        return false; // this will be ignored
}

template<typename conveyor_type, typename handler, typename thread_id, size_t in_barrier_count, bool is_producer>
bool worker_func(conveyor_type& conveyor,
                 barrier_values& barriers,
                 const worker_inst<handler, thread_id, in_barrier_count, is_producer>& w) {
                auto& out_barrier = barriers.values[w.get_out_barrier()].index;
                index_type upper = get_min_index(w.get_in_barriers(), barriers);
                if(is_producer)
                    upper = conveyor.wrap_around(upper);
                index_type lower =  out_barrier.load(std::memory_order_relaxed);
                if(lower < upper) {
                    if(is_producer) {
                        return worker_func_impl(conveyor, barriers, w, out_barrier, upper, lower);
                    } else {
                        worker_func_impl(conveyor, barriers, w, out_barrier, upper, lower);
                    }
                }
                index_type stop = barriers.stop.index.load(std::memory_order_acquire),
                           done = out_barrier.load(std::memory_order_acquire);
                return done < stop;
}

struct empty_thread_func {
    template<typename conveyor_type>
    bool thread_func(conveyor_type*, barrier_values*) const { return false; }
};

template<typename worker_type, typename prev>
class thread_with_id_func : prev {
public:
    thread_with_id_func(const worker_type& w, const prev& p) : prev(p), w(w) {}
    typedef thread_with_id_func<worker_type, prev> type;
    template <typename next_worker>
    thread_with_id_func<next_worker, type> operator()
        (const next_worker& nw) {
        return thread_with_id_func(nw, *this);
    }
    template<typename conveyor_type>
    bool thread_func(conveyor_type* belt, barrier_values* barriers) const {
        // Avoid short-circuit evaluation
        bool prev_result = prev::thread_func(belt, barriers);
        bool this_result = worker_func(*belt, *barriers, w);
        return prev_result || this_result;
    }

    const worker_type& w;
};

template<typename worker_type, typename prev>
thread_with_id_func<worker_type, prev> create_thread_with_id_func(const worker_type& w, const prev& p) {
    return thread_with_id_func<worker_type, prev>(w, p);
}

template<typename worker_type>
thread_with_id_func<worker_type, empty_thread_func> create_thread_with_id_func(const worker_type& w) {
    return thread_with_id_func<worker_type, empty_thread_func>(w, empty_thread_func());
}

template<typename conveyor_type, typename map, typename handler, typename thread_id, size_t in_barrier_count, bool is_producer, bool has_key>
struct map_inserter_impl;

template<typename conveyor_type, typename map, typename handler, typename thread_id, size_t in_barrier_count, bool is_producer>
struct map_inserter_impl<conveyor_type, map, handler, thread_id, in_barrier_count, is_producer, true>
 {
    typedef worker_inst<handler, thread_id, in_barrier_count, is_producer> worker_type;
    typedef typename map::template insert_result<thread_id,
                                             thread_with_id_func<worker_type,
                                                                 typename map::template value_type<thread_id>
                                                                >
                                            > result_type;

    static result_type insert(conveyor_type* conveyor, barrier_values* barriers, const map& tm, const worker_type& w) {
        auto old_value = impl::at<thread_id>::template get(tm);
        return impl::insert<thread_id>(create_thread_with_id_func(w, old_value), tm);
    }

};

template<typename conveyor_type, typename map, typename handler, typename thread_id, size_t in_barrier_count, bool is_producer>
struct map_inserter_impl<conveyor_type, map, handler, thread_id, in_barrier_count, is_producer, false>
 {
    typedef worker_inst<handler, thread_id, in_barrier_count, is_producer> worker_type;
    typedef typename map::template insert_result<thread_id,
                                                  thread_with_id_func<worker_type, empty_thread_func
                                                                      >
                                                     > result_type;
    static result_type insert(conveyor_type* conveyor, barrier_values* barriers, const map& tm, const worker_type& w) {
        return impl::insert<thread_id>(create_thread_with_id_func(w), tm);
    }

};


template<typename conveyor_type, typename map, typename handler, typename thread_id, size_t in_barrier_count, bool is_producer>
struct map_inserter : map_inserter_impl<conveyor_type, map, handler, thread_id, in_barrier_count, is_producer, has_key<thread_id, map>::type::value>
{};


template <typename conveyor_type, typename iter>
void create_thread_func(conveyor_type* belt, barrier_values* barriers, iter iterator, std::deque<std::function<void()> >& thread_funcs) {
        typename iter::value_type element = *iterator;
        std::function<void()> thread_func = [=]() mutable {
            while(element.thread_func(belt, barriers))
            ;
        };
        thread_funcs.push_back(thread_func);
        create_thread_func(belt, barriers, ++iterator, thread_funcs);
}

template <typename conveyor_type>
void create_thread_func(conveyor_type*, barrier_values*, map_iterator<empty_map> iterator,
    std::deque<std::function<void()> >&) {
}

template <typename conveyor_type, typename worker_type>
std::function<void()> make_thread_func(conveyor_type* conveyor, barrier_values* barriers, worker_type& w) {
    return [=]() {
            while(worker_func(*conveyor, *barriers, w))
            ;
        };
}

template <typename conveyor_type, typename map_type>
class thread_creator {
public:
    typedef thread_creator<conveyor_type, map_type> type;

    thread_creator(conveyor_type* conveyor,
        barrier_values* barriers,
        const map_type& thread_map,
        const std::deque<std::function<void()> >& thread_funcs) : conveyor(conveyor), barriers(barriers), thread_map(thread_map), thread_funcs(thread_funcs) {}

    void join() {
        std::deque<std::thread> threads;

        for(auto func : thread_funcs)
            threads.push_back(std::thread(func));

        for (auto& t: threads) t.join();
    }

    auto create_threads() -> decltype(*this) {
        auto iterator = begin(thread_map);
        create_thread_func(conveyor, barriers, iterator, thread_funcs);
        return *this;
    }

    template<typename handler, size_t id, size_t in_barrier_count, bool is_producer> using inserted_type =
        typename map_inserter<conveyor_type, map_type, handler, thread_id<id>, in_barrier_count, is_producer>::result_type;

    template <typename handler, size_t id, size_t in_barrier_count, bool is_producer> using result_type =
        thread_creator<conveyor_type,
                              inserted_type<handler, id, in_barrier_count, is_producer> >;

    template <typename handler, size_t id, size_t in_barrier_count, bool is_producer>
    result_type<handler, id, in_barrier_count, is_producer> operator()
        (const worker_inst<handler, thread_id<id>, in_barrier_count, is_producer>& w) {
       return result_type<handler, id, in_barrier_count, is_producer>
                                              (conveyor, barriers,
                                               map_inserter<conveyor_type,
                                                            map_type,
                                                            handler,
                                                            thread_id<id>,
                                                            in_barrier_count,
                                                            is_producer>::insert(conveyor, barriers, thread_map, w), thread_funcs);
    }

    template <typename handler, size_t in_barrier_count, bool is_producer>
    type operator()
        (const worker_inst<handler, no_thread_id, in_barrier_count, is_producer>& w)  {
        thread_funcs.push_back(make_thread_func(conveyor, barriers, w));
        return *this;
    }

    template <typename left, typename right>
    auto operator() (const seq<left, right>& s) -> decltype(s.apply(*this)) { return s.apply(*this); }

    template <typename top, typename bottom>
    auto operator() (const par<top, bottom>& p) -> decltype(p.apply(*this)) { return p.apply(*this); }

private:
    conveyor_type* conveyor;
    barrier_values* barriers;
    map_type thread_map;
    std::deque<std::function<void()> > thread_funcs;
};

} // namespace impl


} // namespace

#endif // CONVEYOR3_H
