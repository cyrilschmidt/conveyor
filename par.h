#ifndef PAR_H
#define PAR_H

#include "barriers.h"

namespace conveyor {

// Sequential execution of two workers
template <typename top, typename bottom>
class par
{
public:
    template <size_t count, bool is_producer = false> using mapped_type =
    par<typename top::template mapped_type<count, is_producer>, typename bottom::template mapped_type<count, is_producer>>;
    template <size_t count, bool is_producer = false>
    mapped_type<count, is_producer> map(const barriers<count>& in_barriers) const {
        return mapped_type<count, is_producer>(first.map<count, is_producer>(in_barriers), second.map<count, is_producer>(in_barriers));
    }

    static const size_t out_barrier_count = top::out_barrier_count + bottom::out_barrier_count;
    size_t next_barrier() const { return second.next_barrier(); }
    barriers<out_barrier_count> get_out_barriers() const { return first.get_out_barriers()+second.get_out_barriers(); }

    par<top, bottom>& shift(size_t new_barrier) {
        first.shift(new_barrier);
        second.shift(new_barrier);
        return *this;
    }
    par(top first, bottom second) : first(first), second(second) {}

private:
    top first;
    bottom second;
public:
    template <typename visitor>
    auto apply(visitor& v) const -> decltype(v(first)(second)) { return v(first)(second); }
};


template <typename lhs, typename rhs>
par<lhs, rhs>
create_par(lhs f, rhs s) {
    return par<lhs, rhs>(f,s.shift(f.next_barrier()));
}

template <typename lhs, typename rhs>
auto operator + (lhs f, rhs s) -> decltype (create_par(f,s)) { return create_par(f,s); }


}

#endif // PAR_H

