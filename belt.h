#ifndef BELT_H
#define BELT_H

#include <tuple>
#include <boost/mpl/at.hpp>
#include <boost/mpl/size.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/transform.hpp>
#include <boost/mpl/placeholders.hpp>

namespace conveyor {

typedef int index_type;

namespace impl {

template<size_t n>
struct is_power_of_two {
    static const bool value = n && !(n & (n - 1));
};

template <size_t index, typename iterator, typename end_iterator>
struct tuple_maker_impl : tuple_maker_impl<index+1, typename boost::mpl::next<iterator>::type, end_iterator> {
    typedef typename iterator::type value_type;
    value_type value;
};

template<size_t index>
struct getter {
template<typename iterator, typename end_iterator>
static typename tuple_maker_impl<index, iterator, end_iterator>::value_type&
get(tuple_maker_impl<index, iterator, end_iterator>& tmi) {
    return tmi.value;
}
template<typename iterator, typename end_iterator>
static typename tuple_maker_impl<index, iterator, end_iterator>::value_type const&
get(tuple_maker_impl<index, iterator, end_iterator> const& tmi) {
    return tmi.value;
}
};

template <size_t index, typename iterator>
struct tuple_maker_impl<index, iterator, iterator> {
};

template <typename sequence>
struct tuple_maker {
    typedef tuple_maker_impl<0, typename boost::mpl::begin<sequence>::type, typename boost::mpl::end<sequence>::type> type;
};

// Metafunction class that converts a type into an array of that type
template<size_t size>
struct add_extent {
    template<typename item>
    struct apply {
        typedef item type[size];
    };
};

template <typename belt>
class belt_iter_impl {};

} // namespace impl

template <typename belt_type>
class belt_iterator;

template <size_t length, typename sequence>
class belt {
public:
    static_assert(boost::mpl::is_sequence<sequence>::value, "belt requires a sequence of types as template parameter");
    static_assert(impl::is_power_of_two<length>::value, "length must be a power of two");
    static const size_t size = length;
    typedef belt<length, sequence> type;
    typedef belt_iterator<type> iterator;
    index_type wrap_around(index_type index) const { return index+length; }
    template <typename workers_chain>
    void run(const workers_chain&);

    typedef typename boost::mpl::transform<sequence, typename impl::add_extent<length> >::type array_sequence;
    typename impl::tuple_maker<array_sequence>::type data;
    friend class impl::belt_iter_impl<belt<length, sequence> >;
};

namespace impl {

template <typename belt_type>
int operator - (const belt_iter_impl<belt_type>& lhs,
                const belt_iter_impl<belt_type>& rhs);

template <typename belt_type>
bool operator == (const belt_iter_impl<belt_type>& lhs,
                const belt_iter_impl<belt_type>& rhs);

template <size_t length, typename sequence>
class belt_iter_impl<belt<length, sequence> > {
public:
    typedef belt_iter_impl<belt<length, sequence> > type;
    belt_iter_impl(belt<length, sequence>* target, index_type index) : target(target), index(index) {}

    belt_iter_impl<belt<length, sequence> >& swap(belt_iter_impl<belt<length, sequence> >& other) {
        std::swap(target, other.target);
        std::swap(index, other.index);
        return *this;
    }
    belt_iter_impl<belt<length, sequence> >& operator ++() { ++index; return *this; }
    belt_iter_impl<belt<length, sequence> >& operator += (int offset) {
        index += offset;
        return *this;
    }
    bool operator < (const belt_iter_impl<belt<length, sequence> >& other) const { return index < other.index; }

    belt<length, sequence>* target;
    index_type index;
    friend int operator - <>(const belt_iter_impl<belt<length, sequence> >& lhs,
                           const belt_iter_impl<belt<length, sequence> >& rhs);
    friend bool operator == <>(const belt_iter_impl<belt<length, sequence> >& lhs,
                           const belt_iter_impl<belt<length, sequence> >& rhs);

};

template <typename belt_type>
int operator - (const belt_iter_impl<belt_type>& lhs,
                const belt_iter_impl<belt_type>& rhs) {
    return lhs.index - rhs.index;
}

template <typename belt_type>
bool operator == (const belt_iter_impl<belt_type>& lhs,
                const belt_iter_impl<belt_type>& rhs) {
    return lhs.index == rhs.index && lhs.target == rhs.target;
}

} // namespace impl

template<size_t value_index, size_t length, typename sequence>
typename boost::mpl::at<sequence, boost::mpl::int_<value_index> >::type &
    get(impl::belt_iter_impl<belt<length, sequence> >& iter) { return impl::getter<value_index>::get(iter.target->data)[iter.index%length]; }

template <typename belt_type>
int operator - (const belt_iterator<belt_type>& lhs,
                const belt_iterator<belt_type>& rhs);

template <typename belt_type>
bool operator == (const belt_iterator<belt_type>& lhs,
                const belt_iterator<belt_type>& rhs);

template <size_t length, typename sequence>
class belt_iterator<belt<length, sequence> > : public std::iterator<std::random_access_iterator_tag, impl::belt_iter_impl<belt<length, sequence> > > {
public:
    belt_iterator(belt<length, sequence>& target, index_type index) : impl(&target, index) {}
    impl::belt_iter_impl<belt<length, sequence> >& operator * () { return impl; }
    impl::belt_iter_impl<belt<length, sequence> > const& operator * () const { return impl; }
    impl::belt_iter_impl<belt<length, sequence> >* operator -> () { return &impl; }
    impl::belt_iter_impl<belt<length, sequence> > const* operator -> () const { return &impl; }
    belt_iterator<belt<length, sequence> >& operator ++() { ++impl; return *this; }
    bool operator < (const belt_iterator<belt<length, sequence> >& other) const { return impl < other.impl; }
private:
    impl::belt_iter_impl<belt<length, sequence> > impl;
    friend int operator - <>(const belt_iterator<belt<length, sequence> >& lhs,
                           const belt_iterator<belt<length, sequence> >& rhs);
    friend bool operator == <>(const belt_iterator<belt<length, sequence> >& lhs,
                           const belt_iterator<belt<length, sequence> >& rhs);
};

template <typename belt_type>
int operator - (const belt_iterator<belt_type>& lhs,
                const belt_iterator<belt_type>& rhs) {
    return lhs.impl - rhs.impl;
}

template <typename belt_type>
bool operator == (const belt_iterator<belt_type>& lhs,
                const belt_iterator<belt_type>& rhs) {
    return lhs.impl == rhs.impl;
}

template <typename belt_type>
bool operator != (const belt_iterator<belt_type>& lhs,
                const belt_iterator<belt_type>& rhs) {
    return !(lhs == rhs);
}

} // namespace

#endif // BELT_H
