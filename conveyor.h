#ifndef CONVEYOR_H
#define CONVEYOR_H

#include "belt.h"
#include "thread_func.h"

namespace conveyor {

template<size_t length, typename sequence>
template <typename workers_chain>
void belt<length, sequence>::run(const workers_chain& s) {
    auto s2 = s.template map<s.out_barrier_count, true>(s.get_out_barriers());

    impl::barrier_values bar(impl::max_barrier_index()(s2).get_value()+1);

    impl::thread_creator<belt<length, sequence>, impl::empty_map> twic(this, &bar, impl::empty_map(), std::deque<std::function<void()> >());

    twic(s2).create_threads().join();

}


} // namespace

#endif // CONVEYOR_H
