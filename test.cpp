#include <iostream>
#include <gtest/gtest.h>
#include <autocheck/autocheck.hpp>
#include "conveyor.h"

namespace {
    thread_local int tli = 0;
}

using conveyor::get;
using conveyor::worker;
using conveyor::apply;

template <size_t conv_len, typename container, typename func1, typename func2>
bool single_sequence(container& v0, func1 f1, func2 f2) {
    typedef typename container::iterator iterator;
    iterator begin = v0.begin(), end = v0.end();
    typedef decltype(f1(*begin)) value_type1;
    typedef decltype(f2(f1(*begin))) value_type2;

    std::vector<value_type1> v1;
    std::vector<value_type2> v2;
    std::transform(v0.begin(), v0.end(), std::back_inserter(v1), f1);
    std::transform(v1.begin(), v1.end(), std::back_inserter(v2), f2);

    typedef conveyor::belt<conv_len, boost::mpl::vector<value_type1> > conv_belt;
    typedef conveyor::belt_iterator<conv_belt> iter;
    conv_belt b;
    std::vector<value_type2> result;

    auto w1 = conveyor::worker([&](iter i1, iter i2){
        if(begin == end) {
            return std::make_tuple(0, false);
        }

        conveyor::get<0>(*i1) = f1(*begin);
        ++begin;
        return std::make_tuple(1, true);
    });

    auto w2 = conveyor::worker([&](iter i1, iter i2){
        result.push_back(f2(get<0>(*i1)));
        return 1;
    });

    b.run(w1 >> w2);

    return result.size() == v2.size() && std::equal(result.begin(), result.end(), v2.begin());
}

// test for the sequence
// f1 >> (f2 + f3)

template <size_t conv_len, typename container, typename func1, typename func2, typename func3>
bool prop_test_conv1(container& xs, func1 f1, func2 f2, func3 f3) {
    typedef typename container::iterator iterator;
    typedef typename iterator::value_type value_type;
    iterator begin = xs.begin(), end = xs.end();
    typedef decltype(f1(*begin)) value_type1;
    typedef decltype(f2(*begin)) value_type2;
    typedef decltype(f3(f1(*begin),f2(*begin))) result_type;

    std::vector<value_type1> v1;
    std::vector<value_type2> v2;
    std::vector<result_type> rs;
    std::transform(xs.begin(), xs.end(), std::back_inserter(v1), f1);
    std::transform(xs.begin(), xs.end(), std::back_inserter(v2), f2);
    std::transform(v1.begin(), v1.end(), v2.begin(), std::back_inserter(rs), f3);

    typedef conveyor::belt<conv_len, boost::mpl::vector<value_type, value_type1, value_type2> > conv_belt;
    typedef conveyor::belt_iterator<conv_belt> iter;
    conv_belt b;
    std::vector<result_type> result;

    auto w1 = conveyor::worker<2>([&](iter i1, iter i2){
        if(begin == end) {
            return std::make_tuple(0, false);
        }

        tli = 2;
        conveyor::get<0>(*i1) = *begin;
        ++begin;
        return std::make_tuple(1, true);
    });

    auto w2 = conveyor::worker<2>([&](iter i1, iter i2){
        if(tli != 2)
            std::cout << "error: tli=" << tli << ", expected 2\n";
        get<1>(*i1) = f1(get<0>(*i1));
        return 1;
    });

    auto w3 = conveyor::worker([&](iter i1, iter i2){
        get<2>(*i1) = f2(get<0>(*i1));
        return 1;
    });

    auto w4 = conveyor::worker([&](iter i1, iter i2){
        result.push_back(f3(get<1>(*i1),get<2>(*i1)));
        return 1;
    });

    auto w5 = conveyor::worker<1>([](iter i1, iter i2){
        tli = 1;
        return 1;
    });

    auto w6 = conveyor::worker<1>([](iter i1, iter i2){
        if(tli != 1)
            std::cout << "error: tli=" << tli << ", expected 1\n";
        return 1;
    });

    auto s1 = w1 >> (w2 + w3) >> w4 >> w5 >> w6;

    b.run(s1);

    return result.size() == rs.size() && std::equal(result.begin(), result.end(), rs.begin());
}

// test for the sequence
// f1 >> ((f2 >> f3) + (f4 >> f5)) >> f6

template <size_t conv_len, typename container, typename func1, typename func2, typename func3, typename func4, typename func5, typename func6>
bool prop_test_conv2(container& xs, func1 f1, func2 f2, func3 f3, func4 f4, func5 f5, func6 f6) {
    container s1(xs);
    std::transform(xs.begin(), xs.end(), s1.begin(), f1);
    container s2(s1);
    std::transform(s1.begin(), s1.end(), s1.begin(), f2);
    std::transform(s1.begin(), s1.end(), s1.begin(), f3);
    std::transform(s2.begin(), s2.end(), s2.begin(), f4);
    std::transform(s2.begin(), s2.end(), s2.begin(), f5);
    container rs(xs);
    std::transform(s1.begin(), s1.end(), s2.begin(), rs.begin(), f6);

    typedef typename container::iterator iterator;
    iterator begin = xs.begin(), end = xs.end();
    typedef decltype(f1(*begin)) value_type1;
    typedef decltype(f2(f1(*begin))) value_type2;
    typedef decltype(f3(f2(f1(*begin)))) value_type3;
    typedef decltype(f4(f1(*begin))) value_type4;
    typedef decltype(f5(f4(f1(*begin)))) value_type5;
    typedef decltype(f6(f3(f2(f1(*begin))),f5(f4(f1(*begin))))) result_type;

    typedef conveyor::belt<conv_len, boost::mpl::vector<value_type1, value_type2, value_type3, value_type4, value_type5> > conv_belt;
    typedef conveyor::belt_iterator<conv_belt> iter;
    conv_belt b;
    std::vector<result_type> result;

    // each time producer puts a random number of entries into the belt
    std::mt19937 gen(1089);
    std::uniform_int_distribution<> dis(0, xs.size());

    auto w1 = conveyor::worker([&](iter i1, iter i2){
        typename iter::difference_type count = 0;
        if(begin == end)
            return std::make_tuple(count, false);

        typename iter::difference_type limit = dis(gen);
        for(;count < limit && i1 != i2 && begin != end; ++count, ++begin, ++i1) {
            conveyor::get<0>(*i1) = f1(*begin);
        }
        return std::make_tuple(count, true);

    });

    auto w2 = worker(apply<iter, 0, 1>(f2));

    auto w3 = worker(apply<iter, 1, 2>(f3));

    auto w4 = worker(apply<iter, 0, 3>(f4));

    auto w5 = worker(apply<iter, 3, 4>(f5));

    auto w6 = conveyor::worker([&](iter i1, iter i2){
        result.push_back(f6(get<2>(*i1),get<4>(*i1)));
        return 1;
    });

    b.run(w1 >> ((w2 >> w3) + (w4 >> w5)) >> w6);

    return result.size() == rs.size() && std::equal(result.begin(), result.end(), rs.begin());
}

template <size_t conv_len, typename container, typename func1, typename func2, typename func3, typename func4, typename func5, typename func6>
bool prop_test_conv3(container& xs, func1 f1, func2 f2, func3 f3, func4 f4, func5 f5, func6 f6) {
    typedef typename container::iterator iterator;
    iterator begin = xs.begin(), end = xs.end();
    typedef decltype(f1(*begin)) value_type1;
    typedef decltype(f2(f1(*begin))) value_type2;
    typedef decltype(f3(f1(*begin))) value_type3;
    typedef decltype(f4(f1(*begin))) value_type4;
    typedef decltype(f5(f2(f1(*begin)),f3(f1(*begin)))) value_type5;
    typedef decltype(f6(f5(f2(f1(*begin)),f3(f1(*begin))),f4(f1(*begin)))) value_type6;

    std::vector<value_type1> v1;
    std::vector<value_type2> v2;
    std::vector<value_type3> v3;
    std::vector<value_type4> v4;
    std::vector<value_type5> v5;
    std::vector<value_type6> v6;

    std::transform(xs.begin(), xs.end(), std::back_inserter(v1), f1);
    std::transform(v1.begin(), v1.end(), std::back_inserter(v2), f2);
    std::transform(v1.begin(), v1.end(), std::back_inserter(v3), f3);
    std::transform(v1.begin(), v1.end(), std::back_inserter(v4), f4);
    std::transform(v2.begin(), v2.end(), v3.begin(), std::back_inserter(v5), f5);
    std::transform(v5.begin(), v5.end(), v4.begin(), std::back_inserter(v6), f6);

    typedef conveyor::belt<conv_len, boost::mpl::vector<value_type1, value_type2, value_type3, value_type4, value_type5> > conv_belt;
    typedef conveyor::belt_iterator<conv_belt> iter;
    conv_belt b;
    std::vector<value_type6> result;

    auto w1 = conveyor::worker([&](iter i1, iter i2){
        bool go_on = begin != end;
        if(go_on) {
            conveyor::get<0>(*i1) = f1(*begin);
            ++begin;
            return std::make_tuple(1, go_on);
        } else
            return std::make_tuple(0, go_on);

    });

    auto w2 = conveyor::worker([&](iter i1, iter i2){
        get<1>(*i1) = f2(get<0>(*i1));
        return 1;
    });

    auto w3 = conveyor::worker([&](iter i1, iter i2){
        get<2>(*i1) = f3(get<0>(*i1));
        return 1;
    });

    auto w4 = conveyor::worker([&](iter i1, iter i2){
        get<3>(*i1) = f4(get<0>(*i1));
        return 1;
    });

    auto w5 = conveyor::worker([&](iter i1, iter i2){
        get<4>(*i1) = f5(get<1>(*i1),get<2>(*i1));
        return 1;
    });

    auto w6 = conveyor::worker([&](iter i1, iter i2){
        result.push_back(f6(get<4>(*i1),get<3>(*i1)));
        return 1;
    });

    auto w = w1 >> (w2 + w3 + w4) >> w5 >> w6;

    b.run(w);

    return result.size() == v6.size() && std::equal(result.begin(), result.end(), v6.begin());
}

template <typename T = int, typename Property>
void check_container(Property prop, bool verbose = false, size_t n = 100) {
    namespace ac = autocheck;
    ac::check<std::vector<T>>(prop,
        n,
        ac::make_arbitrary<std::vector<T>>(),
        ac::gtest_reporter(),
        ac::classifier<std::vector<T>>(),
        verbose);
}

TEST(Conveyor, SingleSequence1) {
    check_container([](std::vector<int>& input) {
            auto f1 = [&](int i) { return i+5;};
            auto f2 = [&](int i) { return i*7.2;};
            return single_sequence<64>(input, f1, f2);
        }, false, 300);
}

TEST(Conveyor, SimpleSequence1) {
    check_container([](std::vector<int>& input) {
            timespec ts1 = { 0, 200}, ts2 = { 0, 300}, ts3 = {0, 500};
            auto f1 = [&](int i) { nanosleep(&ts1,0); return i+5;};
            auto f2 = [&](int i) { nanosleep(&ts2,0); return i-7;};
            auto f3 = [&](int i, int j) { nanosleep(&ts3,0); return i*j;};
            return prop_test_conv1<4>(input, f1, f2, f3);
        }, false, 300);
}

TEST(Conveyor, SimpleSequence2) {
    check_container([](std::vector<int>& input) {
            auto f1 = [&](int i) {
                timespec ts1 = { 0, i%31*10 };
                nanosleep(&ts1,0); return i%31;};
            auto f2 = [&](int i) {
                timespec ts1 = { 0, i%13*10 };
                nanosleep(&ts1,0); return i%13;};
            auto f3 = [&](int i, int j) {
                timespec ts1 = { 0, (i+j)*10 };
                nanosleep(&ts1,0); return i+j;};
            return prop_test_conv1<16>(input, f1, f2, f3);
        }, false, 300);
}

TEST(Conveyor, Seq2ParSeq) {
    check_container([](std::vector<int>& input) {
            timespec ts1 = { 0, 300}, ts2 = { 0, 500}, ts3 = {0, 200};
            auto f1 = [&](int i) { nanosleep(&ts1,0); return i+5;};
            auto f2 = [&](int i) { nanosleep(&ts2,0); return i+7;};
            auto f3 = [&](int i) { nanosleep(&ts3,0); return i-13;};
            auto f4 = [&](int i) {
                timespec ts4 = { 0, abs(i)%31*10 };
                nanosleep(&ts4,0); return i%31;};
            auto f5 = [&](int i) {
                timespec ts5 = { 0, abs(i)%13*10 };
                nanosleep(&ts5,0); return i%13;};
            auto f6 = [&](int i, int j) {
                timespec ts6 = { 0, abs(i+j)*10 };
                nanosleep(&ts6,0); return i+j;};
            return prop_test_conv2<32>(input, f1, f2, f3, f4, f5, f6);
        }, false, 200);
}

TEST(Conveyor, Seq3ParSeq) {
    check_container([](std::vector<int>& input) {
            timespec ts1 = { 0, 150}, ts2 = { 0, 250}, ts3 = {0, 80};
            auto f1 = [&](int i) { nanosleep(&ts1,0); return i+5;};
            auto f2 = [&](int i) { nanosleep(&ts2,0); return i+7;};
            auto f3 = [&](int i) { nanosleep(&ts3,0); return i-13;};
            auto f4 = [&](int i) {
                timespec ts4 = { 0, abs(i)%31*10 };
                nanosleep(&ts4,0); return i%31;};
            auto f5 = [&](int i, int j) {
                timespec ts5 = { 0, abs(i)%13*10 };
                nanosleep(&ts5,0); return i+3*j;};
            auto f6 = [&](int i, int j) {
                timespec ts6 = { 0, abs(i+j)*10 };
                nanosleep(&ts6,0); return i-j;};
            return prop_test_conv3<8>(input, f1, f2, f3, f4, f5, f6);
        }, false, 100);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
