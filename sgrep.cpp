#include <iostream>
#include <string>
#include <regex>
#include "conveyor.h"

using conveyor::get;
using conveyor::worker;
using conveyor::apply;
using std::string;

int main(int argc, char* argv[]) {
    typedef conveyor::belt<128, boost::mpl::vector<string, bool>> conv_belt;
    typedef conveyor::belt_iterator<conv_belt> iter;

    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " <pattern>\n";
        return 1;
    }

    std::regex regex(argv[1]);

    auto w1 = worker([](iter i1, iter i2) {
        if(std::getline(std::cin, get<0>(*i1)))
            return std::make_tuple(1, true);
        else
            return std::make_tuple(0, false);
    });

    auto w2 = worker(apply<iter, 0, 1>([&](const string& line) {
        return std::regex_search(line, regex);
    }));

    auto w3 = worker([](iter i1, iter i2) {
        if(get<1>(*i1))
            std::cout << get<0>(*i1) << std::endl;
        return 1;
    });

    conv_belt b;
    b.run(w1 >> w2 >> w3);

    return 0;
}
