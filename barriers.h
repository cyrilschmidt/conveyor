#ifndef BARRIERS_H
#define BARRIERS_H

#include <algorithm>
#include <atomic>
#include <memory>

namespace conveyor {

namespace impl {

template<size_t count>
struct barriers {
    static const size_t size = count;
    size_t value[count];
    barriers<count>& operator += (size_t new_barrier) {
        for (size_t &v: value) { v += new_barrier; }
        return *this;
    }
};

template<size_t count_left, size_t count_right>
barriers<count_left+count_right> operator + (const barriers<count_left>& left, const barriers<count_right>& right) {
    barriers<count_left+count_right> result;
    std::copy(left.value, left.value+count_left, result.value);
    std::copy(right.value, right.value+count_right, result.value+count_left);
    return result;
}

struct barrier_values {
    static const size_t padding_size = 64; // TODO: set it according to the hardware
    union padded_value {
        std::atomic<index_type> index;
        char padding[padding_size];
    };
    const size_t size;
    const std::unique_ptr<padded_value[]> values;
    padded_value stop;
    barrier_values(size_t size) : size(size), values(new padded_value[size]) {
        for(size_t i = 0; i < size; ++i)
            values[i].index = 0;
        stop.index = std::numeric_limits<index_type>::max();
    }
};

template <typename barriers_type>
index_type get_min_index(const barriers_type& barriers, const barrier_values& barrier_value) {
    index_type result = barrier_value.values[barriers.value[0]].index.load(std::memory_order_acquire);
    // TODO: remove duplicate reading of barriers.value[0]
    for(size_t barrier: barriers.value) {
        auto value = barrier_value.values[barrier].index.load(std::memory_order_acquire);
        if(value < result)
            result = value;
    }
    return result;
}

} // namespace impl

}

#endif // BARRIERS_H

